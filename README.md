# portfolio-blog-api
Portfolio and personal blog api source code

#### Pipeline Status:
[![pipeline status](https://gitlab.com/andrewtdunn/andrewtdunn-api-devops/badges/main/pipeline.svg)](https://gitlab.com/andrewtdunn/andrewtdunn-api-devops/-/commits/main)

#### Coverage report:
[![coverage report](https://gitlab.com/andrewtdunn/andrewtdunn-api-devops/badges/main/coverage.svg)](https://gitlab.com/andrewtdunn/andrewtdunn-api-devops/-/commits/main)

#### Latest release:
[![Latest Release](https://gitlab.com/andrewtdunn/andrewtdunn-api-devops/-/badges/release.svg)](https://gitlab.com/andrewtdunn/andrewtdunn-api-devops/-/releases)


#### Tests:

1. pytest (unit tests with coverage):

    `dc exec backend pytest -p no:warnings --cov=.`
 
2. flake8 (linting):
    
    `dc exec backend flake8 .`

3. black (code quality):
    - check: `dc exec backend black --check --exclude=migrations .`
    - diff: `dc exec backend black --diff --exclude=migrations .`
    - run: `dc exec backend black --exclude=migrations .`
    
4. isort (sort includes):
    - check: `dc exec backend /bin/sh -c "isort ./*/*.py" --check-only`
    - run: `dc exec backend /bin/sh -c "isort ./*/*.py"`

### To create superuser:

1. ssh to bastion host:
   `ssh ec2_user@<bastion_host> (get host name from pipeline apply stage output)`
2. run the following commands on the instance:
   ```
   sudo usermod -aG docker $USER
   newgrp docker
   ```
3. log in to ECR:
   `$(aws ecr get-login --no-include-email region us-east-1)`
4. Within docker run the django command to create a user:
   ```
   docker run -it \
    -e DB_HOST=<DB_HOST> \
    -e DB_NAME=<DB_NAME> \
    -e DB_USER=<DB_USER> \
    -e DB_PASS=<DB_PASS> \
    <ECR_REPO>:latest \
    sh -c "python manage.py wait_for_db && python manage.py createsuperuser"

   ```
   (secrets can be found in pipeline variables)
5. create a new super user. 


### Architecture

<img src="https://gitlab.com/andrewtdunn/andrewtdunn-api-devops/-/raw/main/andrewtdunn.jpg" width="400" height="485">
