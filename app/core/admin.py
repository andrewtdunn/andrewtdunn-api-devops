from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext as _

from core import models


class UserAdmin(BaseUserAdmin):
    ordering = ["id"]
    list_display = ["email", "name"]
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (_("Personal Info"), {"fields": ("name",)}),
        (_("Permissions"), {"fields": ("is_staff", "is_superuser")}),
        (_("Important dates"), {"fields": ("last_login",)}),
    )
    add_fieldsets = (
        (None, {"classes": ("wide",), "fields": ("email", "password1", "password2")}),
    )


class PictureAdmin(admin.ModelAdmin):
    model = models.Picture
    fieldsets = [
        (
            None,
            {"fields": ["image_tag", "image", "caption", "user"]},
        )
    ]
    readonly_fields = ["image_tag"]
    list_display = ("image_tag", "caption", "image", "created_date", "updated_date")
    search_fields = ["caption"]
    list_per_page = 25
    list_filter = ["created_date", "updated_date"]


admin.site.register(models.User, UserAdmin)
admin.site.register(models.Tag)
admin.site.register(models.Picture, PictureAdmin)
admin.site.register(models.Slideshow)
admin.site.register(models.Blog)
admin.site.register(models.Project)
