variable "prefix" {
  default = "atd"
}

variable "ami" {
  default = "amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "project" {
  default = "andrewtdunn-app-api-devops"
}

variable "contact" {
  default = "andrew@andrewtdunn.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "andrewtdunn-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "742383987475.dkr.ecr.us-east-1.amazonaws.com/andrewtdunn-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "742383987475.dkr.ecr.us-east-1.amazonaws.com/andrewtdunn-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain Name"
  default     = "pandasom.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}